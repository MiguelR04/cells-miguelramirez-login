{
  const {
    html,
  } = Polymer;
  /**
    `<cells-miguelramirez-login>` Description.

    Example:

    ```html
    <cells-miguelramirez-login></cells-miguelramirez-login>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-miguelramirez-login | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsMiguelramirezLogin extends Polymer.Element {

    static get is() {
      return 'cells-miguelramirez-login';
    }

    static get properties() {
      return {
        name: {
          type: String,
          value: ''
        },

        password: {
          type: String,
          value: ''
        }

      };
      
    }

    validar() {
      if (this.name == "miguel" && this.password == "123") {
        //alert("Acceso correcto..");
        //this.set('proplog', true);
        this.dispatchEvent(new CustomEvent('login-success',{detail:this.name, detail:this.password}));
        console.log("Acceso correcto");
        //console.log(this.proplog);

      }
      else {
        this.dispatchEvent(new CustomEvent('login-error',{detail:this.name, detail:this.password}));
        console.log("Error. Acceso incorreccto");
      }
    }



    static get template() {
      return html `
      <style include="cells-miguelramirez-login-styles cells-miguelramirez-login-shared-styles"></style>
      <slot></slot>
      
          

          <div class="login">
          <div class="login-triangle"></div>
    
          <h2 class="login-header">Login</h2>
    
          <form class="login-container">
            <p><input type="text" placeholder="User" name="name" value="{{name::input}}"></p>
            <br>
            <p><input type="password" placeholder="Password" name="password" value="{{password::input}}"></p>
            <br>
            <p><input on-click="validar" type="button" value="Registrar"></p>
          </form>
        </div>
      
      `;
    }
    
  }

  customElements.define(CellsMiguelramirezLogin.is, CellsMiguelramirezLogin);
}